import { useState } from "react";

const List = () => {
  const [listNames, setListNames] = useState([]);
  const [name, setName] = useState("");

  const handleClickInput = () => {
    setListNames([...listNames, name]);
  };

  return (
    <>
      <input
        placeholder="names"
        value={name}
        onChange={(e) => setName(e.target.value)}
      />
      <button onClick={handleClickInput} disabled={!name}>
        Enviar
      </button>
      <ul>
        {listNames.map((name, index) => (
          <li key={index}>{name}</li>
        ))}
      </ul>
    </>
  );
};
export default List;
