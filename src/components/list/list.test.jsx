import { fireEvent, render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import List from ".";

describe("testing component list operation", () => {
  test("Should name appear on the list when the user type a value on input and click on the button", async () => {
    render(<List />);

    userEvent.type(screen.getByRole("textbox"), "ivia");
    userEvent.click(screen.getByRole("button"));

    userEvent.type(screen.getByRole("textbox"), "joao");
    userEvent.click(screen.getByRole("button"));

    userEvent.type(screen.getByRole("textbox"), "rosa");
    userEvent.click(screen.getByRole("button"));

    const listNames = await screen.findAllByRole("listitem");

    expect(listNames).toHaveLength(3);
  });

  test("when the input value is empty, clicking the button will not be allowed", () => {
    render(<List />);
    expect(screen.getByRole("button")).toBeDisabled();
  });

  test("The button can be accessed when input is typed.", () => {
    render(<List />);
    userEvent.type(screen.getByRole("textbox"), "Ivia");
    expect(screen.getByRole("button")).not.toBeDisabled();
  });

  //testando com fireEvent
  test("updates on change", () => {
    render(<List />);
    const input = screen.queryByPlaceholderText("names");
    fireEvent.change(input, { target: { value: "testValue" } });
    expect(input.value).toBe("testValue");
  });
});
